export const API_URL = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';

export function GetFightersEndpoint(): string {
    return 'fighters.json';
}

export function GetFighterByIdEndpoint(id: string): string {
    return `details/fighter/${id}.json`;
}
