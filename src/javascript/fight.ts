import { IFighterInfo } from './interfaces/IFighterInfo';
import { IFighterInfoAfterFight } from './interfaces/IFighterInfoAfterFight';

export function fight(
  firstFighter: IFighterInfo,
  secondFighter: IFighterInfo
): IFighterInfoAfterFight {
  let winner: IFighterInfoAfterFight;
  let rounds: number = 0;

  while (true) {
    rounds++;

    let firstFighterDmg: number = getDamage(firstFighter, secondFighter);
    secondFighter.health -= firstFighterDmg;
    if (secondFighter.health <= 0) {
      winner = firstFighter;
      break;
    }
    let secondFighterDmg: number = getDamage(secondFighter,firstFighter);
    firstFighter.health -= secondFighterDmg;
    if(firstFighter.health <= 0){
      winner = secondFighter;
      break;
    }
  }
  winner.rounds = rounds;
  
  return winner;
}


export function getDamage(attacker: IFighterInfo, enemy: IFighterInfo): number {
  const damage: number = getHitPower(attacker) - getBlockPower(enemy);
  if (damage > 0) {
    return damage;
  } else {
    return 0;
  }
}

export function getHitPower(fighter: IFighterInfo): number {
  // return hit power
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter: IFighterInfo): number {
  // return block power
  return fighter.defense + Math.random() + 1;
}
