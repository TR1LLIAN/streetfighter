import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { IFighter } from './interfaces/IFighter';
import { IFighterInfo } from './interfaces/IFighterInfo';
import { getFighterDetails } from './services/fightersService';

export function createFighters(fighters: IFighter[]): HTMLElement {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements: HTMLElement[] = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

async function showFighterDetails(event: Event, fighter: IFighter): Promise<void> {
  const fullInfo: IFighterInfo = await getFighterInfo(fighter.id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: string): Promise<IFighterInfo> {
  // get fighter form fightersDetailsCache or use getFighterDetails function
  return getFighterDetails(parseInt(fighterId));
}

function createFightersSelector() {
  const selectedFighters = new Map<string, IFighterInfo>();

  return async function selectFighterForBattle(event: Event, fighter: IFighter) {
    const fullInfo = await getFighterInfo(fighter.id);

    if ((event.target as HTMLInputElement).checked) {
      selectedFighters.set(fighter.id, fullInfo);
    } else { 
      selectedFighters.delete(fighter.id);
    }

    if (selectedFighters.size === 2) {

      let mapIterator: IterableIterator<string> = selectedFighters.keys();
      let key1: string = mapIterator.next().value;
      let key2: string = mapIterator.next().value;

      let fighteOne: IFighterInfo = selectedFighters.get(key1);
      let fihtherTwo: IFighterInfo = selectedFighters.get(key2);

      const winner: IFighterInfo = fight(fighteOne, fihtherTwo);
      showWinnerModal(winner);
    }
  }
}
