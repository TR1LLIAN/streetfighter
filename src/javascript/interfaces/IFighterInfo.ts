import { IFighter } from './IFighter';

export interface IFighterInfo extends IFighter {
    health : number;
    attack : number;
    defense : number;
}