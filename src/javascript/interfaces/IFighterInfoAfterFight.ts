import { IFighter } from './IFighter';
import { IFighterInfo } from './IFighterInfo';

export interface IFighterInfoAfterFight extends IFighterInfo, IFighter {
    rounds?: number;
}