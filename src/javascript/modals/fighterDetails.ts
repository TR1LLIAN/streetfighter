import { createElement } from '../helpers/domHelper';
import { IFighterInfo } from '../interfaces/IFighterInfo';
import { showModal } from './modal';

export function showFighterDetailsModal(fighter: IFighterInfo): void {
  const title = 'Fighter info';
  const bodyElement: HTMLElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter: IFighterInfo): HTMLElement {
  const { name, attack, defense, health } = fighter;

  const nameElement = createElement({
    tagName: 'span',
    className: 'fighter-name',
  });
  nameElement.innerText = 'Name: ' + name;

  const imageElement: HTMLImageElement = createElement({
    tagName: 'img',
    className: 'fighter-image',
  }) as HTMLImageElement;
  imageElement.src = fighter.source;

  const healthElement = createElement({
    tagName: 'span',
    className: 'fighter-health',
  });
  healthElement.innerText = '❤️Health: ' + +health.toFixed(2);

  const defenseElement = createElement({
    tagName: 'span',
    className: 'fighter-defense',
  });
  defenseElement.innerText = '🛡️Defense: ' + defense;

  const attackElement = createElement({
    tagName: 'span',
    className: 'fighter-attack',
  });
  attackElement.innerText = '⚔️Attack: ' + attack;

  const statsElement: HTMLElement = createElement({
    tagName: 'div',
    className: 'fighter-stats',
  });
  
  statsElement.append(
    imageElement,
    nameElement,
    healthElement,
    defenseElement,
    attackElement
  );

  return statsElement;
}
