import { createElement } from '../helpers/domHelper';

export function showModal({
  title,
  bodyElement,
}: {
  title: string;
  bodyElement: HTMLElement;
}): void {
  const root: HTMLElement = getModalContainer();
  const modal: HTMLElement = createModal(title, bodyElement);

  root.append(modal);
}

function getModalContainer(): HTMLElement {
  return document.getElementById('root');
}

function createModal(title: string, bodyElement: HTMLElement): HTMLElement {
  const layer = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer = createElement({
    tagName: 'div',
    className: 'modal-root',
  });
  const header = createHeader(title);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string): HTMLElement {
  const headerElement = createElement({
    tagName: 'div',
    className: 'modal-header',
  });
  const titleElement = createElement({ tagName: 'span', className: 'empty' });
  const closeButton = createElement({ tagName: 'div', className: 'close-btn' });

  titleElement.innerText = title;
  closeButton.innerText = 'X';
  closeButton.addEventListener('click', hideModal);
  headerElement.append(title, closeButton);

  return headerElement;
}

function hideModal(event: Event): void {
  const modal: Element = document.getElementsByClassName('modal-layer')[0];
  if(modal.textContent.startsWith('And our winner'))
  {
    modal?.remove();
    window.location.reload();
  }
  else modal?.remove();
  
}
