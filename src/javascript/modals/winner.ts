import { createElement } from '../helpers/domHelper';
import { IFighterInfo } from '../interfaces/IFighterInfo';
import { IFighterInfoAfterFight } from '../interfaces/IFighterInfoAfterFight';
import { showModal } from './modal';

export  function showWinnerModal(fighter:IFighterInfo):void {
  // show winner name and image
  const title: string = `And our winner is ${fighter.name} !!!`;

  const bodyElement: HTMLElement = createWinnerModal(fighter);

  showModal({title, bodyElement});
}

function createWinnerModal(fighter: IFighterInfoAfterFight): HTMLElement{
  const winnerDetails: HTMLElement = createElement({tagName: 'div', className: 'modal-body'});
  const nameElement: HTMLElement = createElement({tagName: 'span', className: 'fighter-name'});

  const imageElement: HTMLImageElement = createElement({tagName: 'img', className: 'fighter-image'}) as HTMLImageElement;
  imageElement.src = fighter.source;
  const spanElement: HTMLElement = createElement({tagName: 'span', className: 'empty' });
  spanElement.innerText = `Totally were ${fighter.rounds} rounds during fight.`;

  const healthElement: HTMLElement = createElement({tagName: 'span', className: 'empty'});
  healthElement.innerText = `Amount of leftover health is ${fighter.health.toFixed(0)}`;

  winnerDetails.append(nameElement, imageElement, spanElement,spanElement, healthElement);

  return winnerDetails;

}