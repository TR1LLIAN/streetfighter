import { callApi } from '../helpers/apiHelper';
import { IFighter } from '../interfaces/IFighter';
import { IFighterInfo } from '../interfaces/IFighterInfo';

export async function getFighters():Promise<IFighter[]> {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');
    
    return apiResult as IFighter[];
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: number): Promise<IFighterInfo> {
  // endpoint - `details/fighter/${id}.json`;
  try{
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET');

    return apiResult as IFighterInfo;
  }
  catch(error){
    throw error;
  }
}

